Template.loginForm.events({
'click .button.radius': function (event){
event.preventDefault();
var email = $('[name=email]').val();
var password = $('[name=password]').val();
Meteor.loginWithPassword(email, password);
Router.go('/cart');  
}
});

Template.main.events({
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
        Router.go('login');
    }
});