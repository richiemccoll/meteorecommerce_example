Router.route('/', {
template: 'shop'
});
Router.configure({
    layoutTemplate: 'main'
});
Router.route('/cart');
Router.route('/about');
Router.route('/faq');
Router.route('/contact');
Router.route('/newCustomer');
Router.route('/login');
Router.route('/checkout');
Router.route('/orderConfirmation');
Router.route('/products/:_id', {
template: 'productPage',
data: function(){
    var currentProduct = this.params._id;
    return Products.findOne({ _id: currentProduct });
  }
});
