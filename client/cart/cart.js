Template.cartItems.helpers({
cartItems: function() {
return CartItems.find().fetch();
}
});

Template.totalCartPrice.helpers({
  sessionCartTotal: function(){
    return SessionCartTotal.find({});
  }
});

Template.cart.events({
'click .button.alert': function(event){
window.confirm('Remove all items from the cart?');
if (confirm) {
event.preventDefault();
Meteor.call('removeCartItems');  
}  
},
'click .logout': function(event){
event.preventDefault();
Meteor.logout();
Router.go('/');  
}  
});
