Template.finalCheckoutPrice.events({
'click .button.radius':function(e){
e.preventDefault();
var total = Session.get("total");
var multipliedTotal = total * 100;
StripeCheckout.open({ 
key: 'pk_test_pvn8v87lBeyuuyM4DuOWcbSm',
amount: multipliedTotal,
name: 'www.example.com',
panelLabel: 'Pay Now',
token: function(res) {
stripeToken = res.id;
console.info(res);
Meteor.call('chargeCard', stripeToken, multipliedTotal);  
}  
});
Router.go('/faq');  
}  
});

Template.checkout.events({
'click .logout': function(event){ 
event.preventDefault();
Meteor.logout();
Router.go('/');    
}
});

Template.finalCheckoutPrice.helpers({
sessionCartTotal: function(){
return SessionCartTotal.find({});
}
});