Products = new Mongo.Collection('products');
CartItems = new Mongo.Collection('cartItems');
SessionCartTotal = new Mongo.Collection('sessionCartTotal');
Customers = new Mongo.Collection('customers');

Meteor.startup(function () {
if (Meteor.isServer) {
CartItems.remove({});
SessionCartTotal.remove({});  
SessionCartTotal.insert({_id: "1", total: 0});  
}
});