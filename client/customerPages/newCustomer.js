Template.newCustomer.events({
'click .button.radius':function(e){
e.preventDefault();  
var firstName = $('input[name=firstName]').val();
var lastName = $('input[name=lastName]').val();
var email = $('input[name=email]').val();
var password = $('input[name=password]').val();  
var firstLineAddress = $('input[name=firstLineAddress]').val();
var secondLineAddress = $('input[name=secondLineAddress]').val();
var zip = $('input[name=zip]').val();
var country = $('select[name=country]').val();
var phoneNumber = $('input[name=phoneNumber]').val();
Customers.insert({firstName: firstName, lastName: lastName, cusEmail: email, firstLineAdd: firstLineAddress, secondLineAdd: secondLineAddress,
                  zip: zip, country: country, phoneNumber: phoneNumber});
Accounts.createUser({email: email,password: password});  
Router.go('cart');  
}
});